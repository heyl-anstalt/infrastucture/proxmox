# Provision: proxmox

## Voraussetzungen

* Die Playbooks wurden für [Debian](https://www.debian.org/index.de.html) und [Ubuntu](https://ubuntu.com/) geschrieben und getestet.
* Eine aktuelle Installation eines der genannten Betriebssysteme ist notwendig.
* Ein Eintrag im DNS muss für den Server existieren.
	* Debian: `/etc/network/interfaces`
	* Ubuntu: `/etc/netplan/00-installer-config.yaml`
* Folgende Software muss zwingend installiert sein:
 	* git
	* ansible
* Die Befehle dafür sind:
	* Debian: `apt update && apt install git ansible -y`
	* Ubuntu: `sudo apt update && sudo apt install git ansible -y`
* Folgende Datei muss angelgt werden, die das Passwort zum Entschlüsseln der [VAULT-Dateien](https://docs.ansible.com/ansible/latest/user_guide/vault.html) enthält. Das Passwort ist im Passwort-Save hinterlegt.
	* /root/ansible.pwd

## Installation

* Debian: `apt update && apt install git ansible -y && ansible-pull -U https://gitlab.com/heyl-anstalt/infrastucture/ansible.git`
* Ubuntu: `sudo apt update && sudo apt install git ansible -y && sudo ansible-pull -U https://gitlab.com/heyl-anstalt/infrastucture/ansible.git`

## Features
### Ansible

* Anlage eines System-Users `ansible`
* Anlage eines Cron-Jobs, der dieses Repository täglich 21:00 ausführt
* Einspielen aller Updates
