#!/bin/bash

FILE=/tmp/.start
MAC="a8:a1:59:46:4d:00"
IP=10.1.1.15

if [ -f "$FILE" ]; then
	echo "DEBUG: load mac from file"
#	MAC=$(cat $FILE)
else
	echo "DEBUG: load from ping"
#	MAC=$(ping -c 1 "$IP" >/dev/null && arp -a | grep "($IP)" | awk '/.*:.*:.*:.*:.*:.*/{print $4}')

	if [ ! -z "$MAC" ]; then
		echo "DEBUG: save to file"
		echo $MAC > $FILE
	fi
fi

if [ ! -z "$MAC" ]; then
	echo "SUCCESS: wakeonlan"
	wakeonlan -f $FILE
else
	echo "ERROR: no mac available"
fi

