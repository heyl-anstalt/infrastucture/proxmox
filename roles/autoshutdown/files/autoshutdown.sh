#!/bin/bash

ip_range=10.1.1 #Netzadresse des eigenen Netzwerkes ohne den Hostanteil
min_ip=200 #check Netz ab ip 100
max_ip=250 #check Netz bis ip 250

_exit () {
  case $1 in
    1)
      echo "Kein Shutdown - Devices online: $2"
      rm -f /tmp/.shutdown
      ;;
    2)
      echo "1" >> /tmp/.shutdown
      TEST=$(cat /tmp/.shutdown | wc -l)
      echo "Kein PC online - Shutdown $TEST"

      if [ "$TEST" -ge "3" ]; then #sobald einer der Hosts auf den ping antwortet, nicht herunterfahren
        rm -f /tmp/.shutdown
        echo "Feierabend"
        shutdown -h now
        #rtcwake -m off -t $(date -d 'tomorrow 05:45' +%s) && echo 'Helau!' 
      fi
      ;;
  esac
  exit $1;
}

i=$(nmap -sP -R ${ip_range}.${min_ip}-${max_ip} | awk '/is up/ {print up}; {gsub (/\(|\)/,""); up = $NF}' | wc -l)

if [ "$i" -ne "0" ]; then #sobald einer der Hosts auf den ping antwortet, nicht herunterfahren
	_exit 1 $i #beenden mit exit 1 (kein shutdown)
fi

_exit 2 # hat kein PC geantwortet, shutdown