#!/bin/bash

IMAGE=noble
IMAGE_NAME=$IMAGE-server-cloudimg-amd64.img
IMAGE_NAME_CUSTOM=ubuntu-cloud-image.img
VMID=9000

function get_checksum() {
  if [ -f SHA256SUMS ]; then
    rm SHA256SUMS
  fi

  wget https://cloud-images.ubuntu.com/${IMAGE}/current/SHA256SUMS

  SHA256SUM=$(cat SHA256SUMS | grep $IMAGE_NAME | awk '{ print $1 }')

  echo $SHA256SUM
}

function download_image() {
  NEW_DOWNLOAD=1

  if [ -f ${IMAGE_NAME} ]; then
    local EXPECTED_CHECKSUM=$(get_checksum)
    FILE_CHECKSUM=$(sha256sum "$IMAGE_NAME" | awk '{ print $1 }')

    if [ "$FILE_CHECKSUM" == "$EXPECTED_CHECKSUM" ]; then
      NEW_DOWNLOAD=0
    fi
  fi

  if [ $NEW_DOWNLOAD == 1 ]; then
    if [ -f ${IMAGE_NAME} ]; then
      rm ${IMAGE_NAME}
      rm ${IMAGE_NAME_CUSTOM}
    fi

    wget https://cloud-images.ubuntu.com/${IMAGE}/current/${IMAGE_NAME}

    cp $IMAGE_NAME $IMAGE_NAME_CUSTOM

    virt-customize -a ${IMAGE_NAME_CUSTOM} --install qemu-guest-agent
    virt-customize -a ${IMAGE_NAME_CUSTOM} --install nfs-common
    virt-customize -a ${IMAGE_NAME_CUSTOM} --install open-iscsi
  fi
}

function create_template() {
  qm destroy $VMID

  qm create $VMID --memory 1024 --net0 virtio,bridge=vmbr0 --scsihw virtio-scsi-pci --name ubuntu-cloud-image
  qm set $VMID --scsi0 local-lvm:0,import-from=/root/${IMAGE_NAME_CUSTOM}
  qm set $VMID --ide2 local-lvm:cloudinit
  qm set $VMID --boot order=scsi0
  qm set $VMID --serial0 socket --vga serial0

  qm template $VMID
}

download_image
create_template
